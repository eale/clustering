from clustering import Clustering


def main():
    with open("data_set.csv", "r") as file:
        data_set = []
        file.readline()  # skip first line with names of rows
        for line in file:
            person_info = line.split()

            person_info = [float(i) for i in person_info]

            data_set.append(person_info)

    cluster_count = 30

    made_cluster = Clustering(array=data_set, cluster_count=cluster_count)

    cluster_content = made_cluster.get_cluster_content()

    made_cluster.get_visualize_clusters(cluster_content)


if __name__ == '__main__':
    main()