import copy
import random

from matplotlib import pyplot as plt


class VisualizeForCluster:
    def _visualize_2d(self, cluster_content):
        k = len(cluster_content)
        plt.grid()
        plt.xlabel("x")
        plt.ylabel("y")

        for i in range(k):
            x_coordinates = []
            y_coordinates = []
            for q in range(len(cluster_content[i])):
                x_coordinates.append(cluster_content[i][q][0])
                y_coordinates.append(cluster_content[i][q][1])
            plt.scatter(x_coordinates, y_coordinates)
        plt.show()

    def _visualize_3d(self, cluster_content):
        ax = plt.axes(projection="3d")
        plt.xlabel("x")
        plt.ylabel("y")

        k = len(cluster_content)

        for i in range(k):
            x_coordinates = []
            y_coordinates = []
            z_coordinates = []
            for q in range(len(cluster_content[i])):
                x_coordinates.append(cluster_content[i][q][0])
                y_coordinates.append(cluster_content[i][q][1])
                z_coordinates.append(cluster_content[i][q][2])
            ax.scatter(x_coordinates, y_coordinates, z_coordinates)
        plt.savefig('foo.png')
        plt.show()

    def get_visualize_clusters(self, cluster_content):
        dict_with_visualize = {2: self._visualize_2d,
                               3: self._visualize_3d}
        dim = len(cluster_content[0][0])
        dict_with_visualize[dim](cluster_content)


class Clustering(VisualizeForCluster):
    def __init__(self, array: list, cluster_count: int, max_cluster_value: int = 100):
        self.array = array
        self.cluster_count = cluster_count
        self.max_cluster_value = max_cluster_value
        self.dim = len(array[0])
        self.array_len = len(array)

    def _data_distribution(self, cluster):
        cluster_content = [[] for i in range(self.cluster_count)]

        for i in range(self.array_len):
            min_distance = float('inf')  # В самом начале задаем ему несоизмеримо большое значение
            situable_cluster = -1
            for j in range(self.cluster_count):  # проходимся по центрам кластеров
                distance = 0
                for q in range(self.dim):
                    distance += (self.array[i][q] - cluster[j][q]) ** 2  # вычисляем дистанцию от точки до центра кластера

                distance **= 0.5
                if distance < min_distance:  # Если расстояние минимальное, тогда присваиваем ее этому кластеру
                    min_distance = distance
                    situable_cluster = j

            cluster_content[situable_cluster].append(self.array[i])

        return cluster_content

    # Пересчет центров кластеров
    def _cluster_update(self, cluster, cluster_content):
        k = len(cluster)
        for i in range(k):
            for q in range(self.dim):
                updated_parameter = 0
                for j in range(len(cluster_content[i])):
                    updated_parameter += cluster_content[i][j][q]
                if len(cluster_content[i]) != 0:
                    updated_parameter = updated_parameter / len(cluster_content[i])
                cluster[i][q] = updated_parameter
        return cluster

    def get_cluster_content(self):
        cluster = [[0 for i in range(self.dim)] for q in range(self.cluster_count)]

        for i in range(self.dim):
            for q in range(self.cluster_count):
                cluster[q][i] = random.randint(0, self.max_cluster_value)  # Задаем первые k центры кластеров рандомно

        cluster_content = self._data_distribution(cluster)

        previous_cluster = copy.deepcopy(cluster)

        # Зацикливаем нахождение новых центров кластеров и распределения точек между ними
        while True:
            cluster = self._cluster_update(cluster, cluster_content)
            cluster_content = self._data_distribution(cluster)
            if cluster == previous_cluster:
                break
            previous_cluster = copy.deepcopy(cluster)

        return cluster_content
